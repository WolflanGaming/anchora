import {computedFrom, inject} from 'aurelia-framework'
import {BindingSignaler} from 'aurelia-templating-resources'
import {OverwatchApi} from 'ow-api'


const API_TARGET = 'http://wolflan.com'

var asyncTimeout = (millis = 0) => new Promise(resolve => setTimeout(resolve, millis))


@inject(BindingSignaler)
export class App {

	constructor(signaler) {
		window.app = this
		// inject aurelie signaler for triggering ui re-render.
		this.signaler = signaler
		this.setup()
	}

	async setup() {
		// setup ui variables
		this.perGroup = 6
		this.showClass = {
			offense: true,
			defense: true,
			tank: true,
			support: true,
		}
		this.voiceChannelId = '399625455935815700' // OW PLAY LOBBY
		// list of all wolflan members
		this.members = new Map
		// list of currently online members
		this.membersInLobby = new Map
		// cache players' ow stats. these won't change within app's timespan.
		this.owAccountsCache = new Map
		// fetch list of wolflan members.
		await this.fetchMemberList()
		// fetch online members and their ow stats.
		await this.refresh()
	}

	@computedFrom('membersInLobby', 'perGroup')
	get groupCount() {
		if (!this.members) return 0
		if (!this.perGroup) return 0
		return this.membersInLobby.size / this.perGroup
	}

	// trigger render
	signal(name) {
		this.signaler.signal(name)
	}

	// fetching

	async refresh() {
		// fetch list of online members in play lobby.
		await this.fetchVoiceChannelList()
		// fetch OW player stats for each member.
		await this.loadOwData()
	}

	async fetchMemberList() {
		this.members.clear()
		var members = await fetch(`${API_TARGET}/members`).then(res => res.json())
		members.forEach(member => this.members.set(member.id, member))
	}

	async fetchVoiceChannelList() {
		this.membersInLobby.clear()
		// TODO: fetch this from ganymede
		var lobby   = await fetch(`${API_TARGET}/voicechannels/${this.voiceChannelId}`).then(res => res.json())
		lobby.memberIds.forEach(id => this.membersInLobby.set(id, this.members.get(id)))
	}

	async loadOwData(forceFresh = false) {
		var memberList = this.membersInLobby.values()
		for (let member of memberList) {
			if (!member.battletag || member.battletag.length === 0)
				continue
			member.battletagMain = member.battletag[0]
			var owStats = this.owAccountsCache.get(member.battletagMain)
			if (owStats) {
				member.owStats = owStats
				this.signal('renderList')
				continue
			}
			OverwatchApi
				.profile(member.battletagMain, 'eu', 'pc', 'quickplay')
				.heroes()
				.then(owStats => {
					member.owStats = owStats
					owStats.accuracy = calculatePlayerAccuracy(owStats)
					owStats.aimTime = calculateAimHeroesPlaytime(owStats)
					this.owAccountsCache.set(owStats.general.tag, owStats)
					this.signal('renderList')
					console.log(`loaded ${member.name}`, owStats.accuracy.toFixed(2), member)
				})
				.catch(err => console.error(`couldn't load ${member.name}`, err))
			await asyncTimeout(150)
		}
	}

}



// [weight, modifier]
const accuracyWeights = {
	ana:		[0.7, 1],
	bastion: 	[1, 1],
	doomfist:	[0.8, 1],
	dva:		[0.8, 1],
	genji:		[1, 1],
	hanzo:		[0.8, 1],
	//junkrat: 	[1, 1],
	lucio:		[0.8, 1.2], // compensation for moving speed and projectile speed
	mccree:		[1, 1],
	//mei:		[1, 1],
	mercy:		[0.5, 1], // even mercy has a pistol
	//moira:	[0.8, 0.8], // compensation for auto-lock & movement
	orisa:		[0.9, 1],
	//pharah:	[1, 1],
	reaper:		[0.7, 1.2], // compensation for close range
	//reinhardt:[1, 1],
	roadhog:	[0.6, 1.2],
	soldier76:	[1, 1],
	sombra:		[1, 1],
	//symmetra: [1, 1],
	//torbjorn: [1, 1],
	tracer:		[1, 1],
	widowmaker:	[0.7, 1],
	//winston: 	[1, 1],
	zarya:		[1, 1],
	zenyatta:	[1, 1.2], // compensation for moving speed
}

function calculatePlayerAccuracy(owStats) {
	var {heroes} = owStats
	var sum = 0
	var div = 0
	Object
		.keys(accuracyWeights)
		.forEach(name => {
			var [weight, modifier] = accuracyWeights[name]
			sum += heroes[name].weaponAccuracy * modifier
			div += weight
		})
	return sum / div
}

function calculateAimHeroesPlaytime() {
	// todo, how much does the player like playing aim based heroes
}