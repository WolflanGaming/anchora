
export function configure(aurelia) {
	aurelia.use
		.basicConfiguration()
		.globalResources('./sort')
	aurelia.start().then(() => aurelia.setRoot())
}