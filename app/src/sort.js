export class SortValueConverter {
	toView(array, propertyName, direction) {
		var factor = direction > 0 ? 1 : -1
		return array
			.slice(0)
			.sort((a, b) => {
				return (a[propertyName] - b[propertyName]) * factor
			});
	}
}

export class FilterClassValueConverter {
	toView(array, showClass) {
		return array.filter(player => showClass[player.mainClass])
	}
}