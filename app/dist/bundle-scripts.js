define('app', ['exports', 'aurelia-framework', 'aurelia-templating-resources', 'ow-api'], function (exports, _aureliaFramework, _aureliaTemplatingResources, _owApi) {
	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.App = undefined;

	function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
		var desc = {};
		Object['ke' + 'ys'](descriptor).forEach(function (key) {
			desc[key] = descriptor[key];
		});
		desc.enumerable = !!desc.enumerable;
		desc.configurable = !!desc.configurable;

		if ('value' in desc || desc.initializer) {
			desc.writable = true;
		}

		desc = decorators.slice().reverse().reduce(function (desc, decorator) {
			return decorator(target, property, desc) || desc;
		}, desc);

		if (context && desc.initializer !== void 0) {
			desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
			desc.initializer = undefined;
		}

		if (desc.initializer === void 0) {
			Object['define' + 'Property'](target, property, desc);
			desc = null;
		}

		return desc;
	}

	var _dec, _dec2, _class, _desc, _value, _class2;

	const API_TARGET = 'http://wolflan.com';

	var asyncTimeout = (millis = 0) => new Promise(resolve => setTimeout(resolve, millis));

	let App = exports.App = (_dec = (0, _aureliaFramework.inject)(_aureliaTemplatingResources.BindingSignaler), _dec2 = (0, _aureliaFramework.computedFrom)('membersInLobby', 'perGroup'), _dec(_class = (_class2 = class App {

		constructor(signaler) {
			window.app = this;
			// inject aurelie signaler for triggering ui re-render.
			this.signaler = signaler;
			this.setup();
		}

		async setup() {
			// setup ui variables
			this.perGroup = 6;
			this.showClass = {
				offense: true,
				defense: true,
				tank: true,
				support: true
			};
			this.voiceChannelId = '399625455935815700'; // OW PLAY LOBBY
			// list of all wolflan members
			this.members = new Map();
			// list of currently online members
			this.membersInLobby = new Map();
			// cache players' ow stats. these won't change within app's timespan.
			this.owAccountsCache = new Map();
			// fetch list of wolflan members.
			await this.fetchMemberList();
			// fetch online members and their ow stats.
			await this.refresh();
		}

		get groupCount() {
			if (!this.members) return 0;
			if (!this.perGroup) return 0;
			return this.membersInLobby.size / this.perGroup;
		}

		// trigger render
		signal(name) {
			this.signaler.signal(name);
		}

		// fetching

		async refresh() {
			// fetch list of online members in play lobby.
			await this.fetchVoiceChannelList();
			// fetch OW player stats for each member.
			await this.loadOwData();
		}

		async fetchMemberList() {
			this.members.clear();
			var members = await fetch(`${API_TARGET}/members`).then(res => res.json());
			members.forEach(member => this.members.set(member.id, member));
		}

		async fetchVoiceChannelList() {
			this.membersInLobby.clear();
			// TODO: fetch this from ganymede
			var lobby = await fetch(`${API_TARGET}/voicechannels/${this.voiceChannelId}`).then(res => res.json());
			lobby.memberIds.forEach(id => this.membersInLobby.set(id, this.members.get(id)));
		}

		async loadOwData(forceFresh = false) {
			var memberList = this.membersInLobby.values();
			for (let member of memberList) {
				if (!member.battletag || member.battletag.length === 0) continue;
				member.battletagMain = member.battletag[0];
				var owStats = this.owAccountsCache.get(member.battletagMain);
				if (owStats) {
					member.owStats = owStats;
					this.signal('renderList');
					continue;
				}
				_owApi.OverwatchApi.profile(member.battletagMain, 'eu', 'pc', 'quickplay').heroes().then(owStats => {
					member.owStats = owStats;
					owStats.accuracy = calculatePlayerAccuracy(owStats);
					owStats.aimTime = calculateAimHeroesPlaytime(owStats);
					this.owAccountsCache.set(owStats.general.tag, owStats);
					this.signal('renderList');
					console.log(`loaded ${member.name}`, owStats.accuracy.toFixed(2), member);
				}).catch(err => console.error(`couldn't load ${member.name}`, err));
				await asyncTimeout(150);
			}
		}

	}, (_applyDecoratedDescriptor(_class2.prototype, 'groupCount', [_dec2], Object.getOwnPropertyDescriptor(_class2.prototype, 'groupCount'), _class2.prototype)), _class2)) || _class);


	// [weight, modifier]
	const accuracyWeights = {
		ana: [0.7, 1],
		bastion: [1, 1],
		doomfist: [0.8, 1],
		dva: [0.8, 1],
		genji: [1, 1],
		hanzo: [0.8, 1],
		//junkrat: 	[1, 1],
		lucio: [0.8, 1.2], // compensation for moving speed and projectile speed
		mccree: [1, 1],
		//mei:		[1, 1],
		mercy: [0.5, 1], // even mercy has a pistol
		//moira:	[0.8, 0.8], // compensation for auto-lock & movement
		orisa: [0.9, 1],
		//pharah:	[1, 1],
		reaper: [0.7, 1.2], // compensation for close range
		//reinhardt:[1, 1],
		roadhog: [0.6, 1.2],
		soldier76: [1, 1],
		sombra: [1, 1],
		//symmetra: [1, 1],
		//torbjorn: [1, 1],
		tracer: [1, 1],
		widowmaker: [0.7, 1],
		//winston: 	[1, 1],
		zarya: [1, 1],
		zenyatta: [1, 1.2] };

	function calculatePlayerAccuracy(owStats) {
		var { heroes } = owStats;
		var sum = 0;
		var div = 0;
		Object.keys(accuracyWeights).forEach(name => {
			var [weight, modifier] = accuracyWeights[name];
			sum += heroes[name].weaponAccuracy * modifier;
			div += weight;
		});
		return sum / div;
	}

	function calculateAimHeroesPlaytime() {
		// todo, how much does the player like playing aim based heroes
	}
});
define('config', ['exports'], function (exports) {
	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.configure = configure;
	function configure(aurelia) {
		aurelia.use.basicConfiguration().globalResources('./sort');
		aurelia.start().then(() => aurelia.setRoot());
	}
});
define('ow-api', ['exports'], function (exports) {
	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	const RANK = {
		'1': 'bronze',
		'2': 'silver',
		'3': 'gold',
		'4': 'platinum',
		'5': 'diamond',
		'6': 'master',
		'7': 'grandmaster'
	};

	var topHeroesCategories = {
		'overwatch.guid.0x0860000000000021': 'timePlayed',
		'overwatch.guid.0x0860000000000039': 'gamesWon',
		'overwatch.guid.0x086000000000002F': 'weaponAccuracy',
		'overwatch.guid.0x08600000000003D2': 'eliminationsPerLife',
		'overwatch.guid.0x0860000000000346': 'multikill',
		'overwatch.guid.0x086000000000039C': 'objectiveKills'
	};
	var achievementCategories = {
		'overwatch.achievementCategory.0': 'general',
		'overwatch.achievementCategory.1': 'offense',
		'overwatch.achievementCategory.2': 'defense',
		'overwatch.achievementCategory.3': 'tank',
		'overwatch.achievementCategory.4': 'support',
		'overwatch.achievementCategory.5': 'maps',
		'overwatch.achievementCategory.6': 'special'
	};

	var careerStatsPlurals = {
		'meleeFinalBlow': 'meleeFinalBlows',
		'soloKill': 'soloKills',
		'soloKillMostInGame': 'soloKillsMostInGame',
		'objectiveKill': 'objectiveKills',
		'objectiveKillMostInGame': 'objectiveKillsMostInGame',
		'finalBlow': 'finalBlows',
		'finalBlowMostInGame': 'finalBlowsMostInGame',
		'elimination': 'eliminations',
		'eliminationMostInGame': 'eliminationsMostInGame',
		'enviromentalKill': 'enviromentalKills',
		'multikill': 'multikills',
		'death': 'deaths',
		'enviromentalDeath': 'enviromentalDeaths',
		'card': 'cards',
		'medal': 'medals',
		'teleporterPadDestroyed': 'teleporterPadsDestroyed',
		'offensiveAssist': 'offensiveAssists',
		'offensiveAssistMostInGame': 'offensiveAssistsMostInGame',
		'defensiveAssist': 'defensiveAssists',
		'defensiveAssistMostInGame': 'defensiveAssistsMostInGame'
	};

	var offenseHeroes = ['genji', 'mccree', 'pharah', 'reaper', 'soldier76', 'sombra', 'tracer', 'doomfist'];
	var defenseHeroes = ['bastion', 'hanzo', 'junkrat', 'mei', 'torbjorn', 'widowmaker'];
	var tankHeroes = ['dva', 'reinhardt', 'roadhog', 'winston', 'zarya', 'orisa'];
	var supportHeroes = ['ana', 'lucio', 'mercy', 'symmetra', 'zenyatta', 'moira'];

	var ID = new Map();
	var NAME = new Map();
	var SLUG = new Map();
	var IDS = [];
	var NAMES = [];
	var SLUGS = [];

	function createHeroDatabase(body) {
		if (SLUG.size !== 0) return;
		var container = body.querySelector('[data-category-id="overwatch.guid.0x0860000000000021"]');
		Array.from(container.children).forEach(div => {
			var id = div.dataset.heroGuid;
			var name = div.getElementsByClassName('title')[0].textContent;
			var slug = sanitizeSlug(name);
			ID.set(name, id);
			ID.set(slug, id);
			NAME.set(id, name);
			NAME.set(slug, name);
			SLUG.set(name, slug);
			SLUG.set(id, slug);
			IDS.push(id);
			NAMES.push(name);
			SLUGS.push(slug);
		});
	}

	function replaceDiacritics(string) {
		var diacritics = [/[\300-\306]/g, /[\340-\346]/g, // A, a
		/[\310-\313]/g, /[\350-\353]/g, // E, e
		/[\314-\317]/g, /[\354-\357]/g, // I, i
		/[\322-\330]/g, /[\362-\370]/g, // O, o
		/[\331-\334]/g, /[\371-\374]/g, // U, u
		/[\321]/g, /[\361]/g, // N, n
		/[\307]/g, /[\347]/g];
		var chars = ['A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u', 'N', 'n', 'C', 'c'];
		for (var i = 0; i < diacritics.length; i++) string = string.replace(diacritics[i], chars[i]);
		return string;
	}
	function sanitizeSlug(string) {
		return replaceDiacritics(string).toLowerCase().match(/\w|\d/g).join('');
	}

	// ---------------------- UTILS --------------------------------------------

	function mapObject(array, callback) {
		var output = {};
		for (var i = 0; i < array.length; i++) {
			Object.assign(output, callback(array[i]));
		}
		return output;
	}

	function camelCase(str) {
		return str.replace(/[^A-Za-z ]/g, '').replace(/(?:^\w|[A-Z]|\b\w)/g, (letter, index) => {
			return index === 0 ? letter.toLowerCase() : letter.toUpperCase();
		}).replace(/\s+/g, '');
	}

	function timeToNumber(timeString) {
		return timeString.split(':').reverse().reduce((acc, value, index) => {
			return Number(acc) + Number(value) * 60 ** index;
		});
	}

	// -------------------------- API UTILS ---------------------------------------

	function createEmptyHeroList() {
		var heroes = {};
		SLUGS.slice(0).forEach(slug => {
			var id = ID.get(slug);
			var name = NAME.get(slug);
			heroes[slug] = {
				slug,
				id,
				name,
				// adding these stats cause Sombra sometimes doesn't appear in the list
				timePlayed: 0
			};
		});
		return heroes;
	}

	// Blizzard doesn't provide total games played number in quickplay
	// and we have to guess it based on average stats
	function calculateGamesPlayed(stats) {
		var factors = [];

		if (stats.meleeFinalBlows > 0) factors.push(stats.meleeFinalBlows / stats.meleeFinalBlowsAverage);
		if (stats.timeSpentOnFire) factors.push(timeToNumber(stats.timeSpentOnFire) / timeToNumber(stats.timeSpentOnFireAverage));
		if (stats.soloKills > 0) factors.push(stats.soloKills / stats.soloKillsAverage);
		if (stats.objectiveTime) factors.push(timeToNumber(stats.objectiveTime) / timeToNumber(stats.objectiveTimeAverage));
		if (stats.objectiveKills > 0) factors.push(stats.objectiveKills / stats.objectiveKillsAverage);
		if (stats.healingDone > 0) factors.push(stats.healingDone / stats.healingDoneAverage);
		if (stats.finalBlows > 0) factors.push(stats.finalBlows / stats.finalBlowsAverage);
		if (stats.deaths > 0) factors.push(stats.deaths / stats.deathsAverage);
		if (stats.damageDone > 0) factors.push(stats.damageDone / stats.damageDoneAverage);
		if (stats.eliminations > 0) factors.push(stats.eliminations / stats.eliminationsAverage);

		var sum = 0;
		for (var i = 0; i < factors.length; i++) sum += factors[i];
		stats.gamesPlayed = Math.round(sum / factors.length);
		stats.gamesLost = stats.gamesPlayed - stats.gamesWon;
	}

	function calculateWinrate(wins, losses) {
		var sum = wins + losses;
		return wins / sum * 100;
	}

	// ---------------------- PARSER --------------------------------------------

	function parseFeatured(node) {
		var name = node.firstElementChild.lastElementChild.lastElementChild.textContent;
		name = camelCase(name);
		var value = node.firstElementChild.lastElementChild.firstElementChild.textContent;
		return { [name]: value };
	}

	function parseTopHeroes(node, heroesList) {
		var categoryName = topHeroesCategories[node.getAttribute('data-category-id')];
		Array.from(node.children).map(node => parseTopHeroesItem(node, categoryName, heroesList));
	}
	function parseTopHeroesItem(node, categoryName, heroesList) {
		var id = node.firstChild.src.slice(-22, -4);
		var slug = SLUG.get(id);
		var heroObject = heroesList[slug];
		var percentage = Number(node.getAttribute('data-overwatch-progress-percent'));
		var originalValue = node.lastElementChild.lastElementChild.lastElementChild.textContent;
		if (originalValue.includes('minutes')) var value = parseInt(originalValue) / 60;else var value = parseFloat(originalValue);
		if (Number.isNaN(value)) value = 0;
		heroObject[categoryName + 'Percentage'] = percentage;
		heroObject[categoryName] = value;
	}

	function parseCareerStats(items) {
		var name = camelCase(items.firstElementChild.textContent.trim());
		// fixes non-plural names for players with low playtime
		name = careerStatsPlurals[name];
		var value = items.lastElementChild.textContent.trim();
		// convert all numeric value to number type
		if (!value.includes(':') && name !== 'timePlayed') value = Number(value.replace(/,/g, ''));
		if (Number.isNaN(value)) value = 0;
		return { [name]: value };
	}

	function parseAchivements(node) {
		var categoryName = achievementCategories[node.getAttribute('data-category-id')];
		return {
			[categoryName]: mapObject(node.firstElementChild.children, parseAchivement)
		};
	}
	function parseAchivement(node) {
		node = node.firstElementChild;
		var achivementName = camelCase(node.textContent.trim());
		if (achivementName === '') return {};
		return {
			[achivementName]: !node.className.includes('m-disabled')
		};
	}

	// ----------------------- API ---------------------------------------------


	let OverwatchApi = exports.OverwatchApi = class OverwatchApi {

		constructor(tag, region, platform = 'pc', mode = 'combined') {
			this._tag = tag.trim();
			tag = encodeURIComponent(tag.replace('#', '-'));
			region = encodeURIComponent(region);
			platform = encodeURIComponent(platform);
			this.onLoad = this.onLoad.bind(this);
			this._mode = mode;
			if (location.host === 'playoverwatch.com') {
				// Injected into iframe
				this.onLoad(document.body.innerHTML);
				this.loaded = Promise.resolve();
				// todo: return data
			} else {
				// Used as a library in app.
				var url = `https://playoverwatch.com/en-us/career/${platform}/${region}/${tag}`;
				this.loaded = fetch(url).then(res => res.text()).then(this.onLoad).catch(err => console.error(`can't fetch ${platform}/${region}/${tag}`, err));
			}
		}

		static profile(tag, region, platform, mode) {
			return new Chainable(tag, region, platform, mode);
		}

		onLoad(data) {
			this._body = document.createRange().createContextualFragment(data);
			var h1 = this._body.querySelector('h1');
			if (h1 && h1.textContent.toLowerCase().includes('not found')) throw new Error('Profile does not exists');
			createHeroDatabase(this._body);
			if (this._mode !== 'combined') this._modebody = this._body.querySelector(`#${this._mode}`);
		}

		general(stats, heroes) {
			var level = parseInt(this._body.querySelector('.player-level').textContent);
			var name = this._tag.split('#')[0].trim();
			var tag = this._tag;
			var general = { level, name, tag };
			var $rank = this._body.querySelector('.competitive-rank');
			if ($rank) {
				general.rank = $rank && parseInt($rank.textContent);
				general.rankTier = $rank && parseInt($rank.firstElementChild.src.slice(-5, -4));
				general.rankName = RANK[general.rankTier];
				general.competitive = true;
			} else {
				general.competitive = false;
				general.rankTier = 0;
			}
			if (stats) {
				if (!stats.gamesPlayed) calculateGamesPlayed(stats);
				// TODO: mixed - both quickplay and competitive
				general.timePlayed = parseInt(stats.timePlayed);
				general.games = stats.gamesPlayed;
				general.wins = stats.gamesWon;
				general.losses = stats.gamesLost;
				general.winRate = calculateWinrate(stats.gamesWon, stats.gamesLost);
				//hasCompetitive
			}
			if (heroes) {
				// calculate time played on heroes and classes
				function getTimePlayed(name) {
					return heroes[name].timePlayed;
				}
				function reduceTimePlayed(current, next) {
					return current + next;
				}
				var offenseTime = offenseHeroes.map(getTimePlayed).reduce(reduceTimePlayed);
				var defenseTime = defenseHeroes.map(getTimePlayed).reduce(reduceTimePlayed);
				var tankTime = tankHeroes.map(getTimePlayed).reduce(reduceTimePlayed);
				var supportTime = supportHeroes.map(getTimePlayed).reduce(reduceTimePlayed);
				general.timePlayed = offenseTime + defenseTime + tankTime + supportTime;
				general.timePlayedOffense = offenseTime;
				general.timePlayedDefense = defenseTime;
				general.timePlayedTank = tankTime;
				general.timePlayedSupport = supportTime;
				// calculate main class
				var playTimeNames = ['timePlayedOffense', 'timePlayedDefense', 'timePlayedTank', 'timePlayedSupport'];
				general.mainClass = playTimeNames.sort((a, b) => general[b] - general[a]).shift().slice(10).toLowerCase();
				// calculate main heroes
				general.mainHeroes = Object.values(heroes).sort((a, b) => b.timePlayed - a.timePlayed).slice(0, 5).map(hero => hero.slug);
			}
			return general;
		}

		featured() {
			var items = this._modebody.firstElementChild.firstElementChild.children[2].children;
			return mapObject(items, parseFeatured);
		}

		// get all heroes by default
		careerStats(id = '0x02E00000FFFFFFFF') {
			var selector = `div[data-group-id="stats"][data-category-id="${id}"]`;
			var target = this._modebody.querySelector(selector);
			var items = target.querySelectorAll('tbody tr');
			var stats = mapObject(items, parseCareerStats);
			if (stats.meleeFinalBlows) return stats;
		}

		achivements() {
			var nodes = this._body.querySelectorAll('div[data-group-id="achievements"]');
			return mapObject(nodes, parseAchivements);
		}

		heroes(...names) {
			if (names.length === 0) {
				var heroes = createEmptyHeroList();
				Array.from(this._modebody.querySelectorAll(`div[data-group-id="comparisons"]`)).forEach(node => parseTopHeroes(node, heroes));
				return heroes;
			} else if (names.length > 1) return names.map(name => this.hero(name));else return this.hero(name);
		}

		hero(name) {
			return {};
		}

	};
	let Chainable = class Chainable extends OverwatchApi {

		constructor(...args) {
			super(...args);
			this._output = {};
			this._tasks = [];
			this.general();
			this.promise = this.loaded.then(() => {
				var task;
				while (task = this._tasks.pop()) task();
				return this._output;
			});
		}

		featured(...args) {
			this._tasks.push(() => this._output.featured = super.featured(...args));
			return this;
		}

		heroes(...names) {
			this._tasks.push(() => this._output.heroes = super.heroes(...names));
			return this;
		}

		// TODO
		heroesComplex(...args) {
			console.warn('heroesComplex NOT IMPLEMENTED YET');
			return this;
		}

		careerStats(...args) {
			this._tasks.push(() => this._output.careerStats = super.careerStats(...args));
			return this;
		}

		achivements(...args) {
			this._tasks.push(() => this._output.achivements = super.achivements(...args));
			return this;
		}

		general() {
			this._tasks.push(() => {
				this._output.general = super.general(this._output.careerStats, this._output.heroes);
			});
			return this;
		}

		then(resolve, reject) {
			return this.promise.then(resolve, reject);
		}
		catch(reject) {
			return this.promise.catch(reject);
		}

	};
});
define("sort", ["exports"], function (exports) {
	"use strict";

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	let SortValueConverter = exports.SortValueConverter = class SortValueConverter {
		toView(array, propertyName, direction) {
			var factor = direction > 0 ? 1 : -1;
			return array.slice(0).sort((a, b) => {
				return (a[propertyName] - b[propertyName]) * factor;
			});
		}
	};
	let FilterClassValueConverter = exports.FilterClassValueConverter = class FilterClassValueConverter {
		toView(array, showClass) {
			return array.filter(player => showClass[player.mainClass]);
		}
	};
});
//# sourceMappingURL=bundle-scripts.js.map
