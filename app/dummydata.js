
var dummyData = [{
	id: '1',
	name: 'Mike',
	battletag: ['Mike#25749'],
	battletagMain: 'Mike#25749'
}, {
	id: '2',
	name: 'Catalyst',
	battletag: ['Catalyst#21394'],
	battletagMain: 'Catalyst#21394'
}, {
	id: '3',
	name: 'Somebody',
	battletag: ['Ritis#21503'],
	battletagMain: 'Ritis#21503'
}, {
	id: '4',
	name: 'A Blender',
	battletag: ['Blender#21426'],
	battletagMain: 'Blender#21426'
}]