# Wolflan Gaming: Project Anchora

Player rating & match making system for Wolflan's Overwatch gatherings.

## Installation

```
clone repo
```

Installing dependencies.

```
npm install
```

Download and unpack latest stable [NWJS](https://nwjs.io/) (SDK version) into the root of anchora folder (`nw.exe` and all other files has to be next to `package.json` and ˙/app˙)

## Running

Launch `nw.exe` (or whatever you rename it to)

## Development

Press `f12` to open Dev Tools.

Running builtasks - endless transpilation of source code from ˙/app/src˙ to `/app/dist`.

```
gulp
```

